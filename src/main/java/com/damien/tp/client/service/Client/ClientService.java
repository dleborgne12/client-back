package com.damien.tp.client.service.Client;

import com.damien.tp.client.api.Client.SearchClientInput;
import com.damien.tp.client.domain.Client.Client;
import com.damien.tp.client.repository.Client.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class ClientService {

  private final ClientRepository clientRepository;

  @Autowired
  public ClientService(ClientRepository clientRepository) {
    this.clientRepository = clientRepository;
  }

  public List<Client> findAllClients(int max) {
    Pageable limit = PageRequest.of(0, max);
    return this.clientRepository.findAll(limit).toList();
  }

  public List<Client> findAllByFilter(SearchClientInput clientInput) {
    int maxClients;
    List<Client> clients = convertToClients(clientInput);
    maxClients = returnMaxUserAvailable(clientInput, clients);
    return clients.subList(0, maxClients);
  }

  private int returnMaxUserAvailable(SearchClientInput clientInput, List<Client> clients) {
    int maxClients;
    if (clients.size() <= clientInput.getMaxUser()) {
      maxClients = clients.size();
    } else {
      maxClients = clientInput.getMaxUser();
    }
    return maxClients;
  }

  private List<Client> convertToClients(SearchClientInput clientInput) {
    List<Client> clients =
        this.clientRepository.findByFilters(
            clientInput.getAdresse().toLowerCase(),
            clientInput.getCodePostal().toLowerCase(),
            clientInput.getNom().toLowerCase(),
            clientInput.getPrenom().toLowerCase(),
            clientInput.getVille().toLowerCase());
    return clients;
  }

  public List<String> getListName(String value) {
    List<String> listName = this.clientRepository.findListByName(value.toLowerCase());
    if(listName.isEmpty()) {
      return new ArrayList<>();
    }else{
      return listName.subList(0, this.getListSizeMax5(listName.size()));
    }
  }

  public List<String> getListFirstname(String value) {
    List<String> listName = this.clientRepository.findListByFirstname(value.toLowerCase());
    if(listName.isEmpty()) {
      return new ArrayList<>();
    }else{
      return listName.subList(0, this.getListSizeMax5(listName.size()));
    }
  }

  public List<String> getListAddress(String value) {
    List<String> listName = this.clientRepository.findListByAddress(value.toLowerCase());
    if(listName.isEmpty()) {
      return new ArrayList<>();
    }else{
      return listName.subList(0, this.getListSizeMax5(listName.size()));
    }
  }

  public List<String> getListCity(String value) {
    List<String> listName = this.clientRepository.findListByCity(value.toLowerCase());
    if(listName.isEmpty()) {
      return new ArrayList<>();
    }else{
      return listName.subList(0, this.getListSizeMax5(listName.size()));
    }
  }

  public List<String> getListZipCode(String value) {
    List<String> listName = this.clientRepository.findListByZipCode(value.toLowerCase());
    if(listName.isEmpty()) {
      return new ArrayList<>();
    }else{
      return listName.subList(0, this.getListSizeMax5(listName.size()));
    }
  }

  public Client addOrUpdateClient(Client client) {
    return this.clientRepository.save(client);
  }

  public void deleteClient(UUID id) {
    this.clientRepository.deleteById(id);
  }

  private int getListSizeMax5(int max) {
    if(max >= 5) {
      return 5;
    }else{
      return max;
    }
  }
}
