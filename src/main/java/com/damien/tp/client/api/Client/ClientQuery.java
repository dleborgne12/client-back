package com.damien.tp.client.api.Client;

import com.damien.tp.client.domain.Client.Client;
import com.damien.tp.client.service.Client.ClientService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/client")
@AllArgsConstructor
public class ClientQuery {

  private final ClientService clientService;

  @PutMapping("/all")
  public ResponseEntity<List<Client>> getAllClients(@RequestBody int max) {
    List<Client> clients = this.clientService.findAllClients(max);
    return new ResponseEntity<>(clients, HttpStatus.OK);
  }

  @PostMapping("/findByFilters")
  public ResponseEntity<List<Client>> getAllClientsByFilters(
      @RequestBody SearchClientInput clientInput) {
    System.out.println(clientInput);
    List<Client> clients = this.clientService.findAllByFilter(clientInput);
    return new ResponseEntity<>(clients, HttpStatus.OK);
  }

  @PutMapping("/addorupdate")
  public ResponseEntity<Client> addClient(@RequestBody ClientInput client) {
    Client newClient = this.clientService.addOrUpdateClient(this.convertToClient(client));
    return new ResponseEntity<>(newClient, HttpStatus.OK);
  }

  @DeleteMapping("/delete/{id}")
  public ResponseEntity<?> deleteClient(@PathVariable("id") UUID id) {
    this.clientService.deleteClient(id);
    return new ResponseEntity<>(HttpStatus.OK);
  }

  public Client convertToClient(ClientInput clientInput) {
    return Client.builder()
        .id(clientInput.getId())
        .nom(clientInput.getNom())
        .prenom(clientInput.getPrenom())
        .adresse(clientInput.getAdresse())
        .ville(clientInput.getVille())
        .codePostal(clientInput.getCodePostal())
        .build();
  }

  @GetMapping("/getName/{value}")
  public List<String> getListName(@PathVariable String value) {
    return this.clientService.getListName(value);
  }
  @GetMapping("/getFirstName/{value}")
  public List<String> getListFirstname(@PathVariable String value) {
    return this.clientService.getListFirstname(value);
  }
  @GetMapping("/getAddress/{value}")
  public List<String> getListAddress(@PathVariable String value) {
    return this.clientService.getListAddress(value);
  }
  @GetMapping("/getCity/{value}")
  public List<String> getListCity(@PathVariable String value) {
    return this.clientService.getListCity(value);
  }
  @GetMapping("/getZipCode/{value}")
  public List<String> getListZipCode(@PathVariable String value) {
    return this.clientService.getListZipCode(value);
  }
}
