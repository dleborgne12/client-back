package com.damien.tp.client.api.Client;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SearchClientInput {
  private int maxUser;
  private String nom;
  private String prenom;
  private String adresse;
  private String ville;
  private String codePostal;
}
