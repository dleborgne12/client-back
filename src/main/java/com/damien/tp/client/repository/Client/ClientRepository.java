package com.damien.tp.client.repository.Client;

import com.damien.tp.client.domain.Client.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface ClientRepository extends JpaRepository<Client, UUID> {

  @Query(
      "select c from Client c where lower(c.adresse) LIKE %:adresse% AND lower(c.codePostal) LIKE %:code_postal% AND lower(c.nom) LIKE %:nom% AND lower(c.prenom) LIKE %:prenom% AND lower(c.ville) LIKE %:ville%")
  List<Client> findByFilters(
      String adresse, String code_postal, String nom, String prenom, String ville);

  @Query("select distinct c.nom from Client c where lower(c.nom) like :value%")
  List<String> findListByName(String value);

  @Query("select distinct c.prenom from Client c where lower(c.prenom) like :value%")
  List<String> findListByFirstname(String value);

  @Query("select distinct c.adresse from Client c where lower(c.adresse) like :value%")
  List<String> findListByAddress(String value);

  @Query("select distinct c.ville from Client c where lower(c.ville) like :value%")
  List<String> findListByCity(String value);

  @Query("select distinct c.codePostal from Client c where lower(c.codePostal) like :value%")
  List<String> findListByZipCode(String value);
}
