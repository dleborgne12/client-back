package com.damien.tp.client.demoSet;

import com.damien.tp.client.demoSet.Client.ClientDemo;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Profile("generate-demo")
public class MainDemo implements CommandLineRunner {

  private final ClientDemo clientDemo;

  @Override
  public void run(String... args) {
    clientDemo.init();
  }
}
