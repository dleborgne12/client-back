package com.damien.tp.client.demoSet.Client;

import com.damien.tp.client.domain.Client.Client;
import com.damien.tp.client.service.Client.ClientService;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

@Slf4j
@Component
@RequiredArgsConstructor
public class ClientDemo {
  private final ClientService clientService;
  private final ObjectMapper objectMapper;

  @Value("classpath:data/clients.csv")
  Resource resource;

  public void init() {
    try {
      log.info("Generation de la base de données");
      try (BufferedReader br = new BufferedReader(new FileReader(resource.getFile()))) {
        String line;
        while ((line = br.readLine()) != null) {
          String[] values = line.split(";");
          Client client = ClientMapperCsv.CsvToClient(values);
          this.clientService.addOrUpdateClient(client);
        }
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
