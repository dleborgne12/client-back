package com.damien.tp.client.demoSet.Client;

import com.damien.tp.client.domain.Client.Client;

import java.util.UUID;

public class ClientMapperCsv {
  public static Client CsvToClient(String[] args) {
    Client client =
        Client.builder()
            .id(UUID.fromString(args[0]))
            .nom(args[1])
            .prenom(args[2])
            .adresse(args[3])
            .ville(args[4])
            .codePostal(args[5])
            .build();
    return client;
  }
}
