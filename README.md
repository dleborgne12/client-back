# Client-Front

TP realisé pour le cours ServiceWeb.
C'est la partie front du projet Client-APP'

Back-end: https://gitlab.com/dleborgne12/client-back/ Utilisation de Spring Boot, PostgreSQL, utilisation d'une image docker

Front-end: https://gitlab.com/dleborgne12/client-front/ Utilisation d'angular.

Ecrire les clients dans la base (une fois docker-compose exécuté):

Recupérez l'id du container: ```docker.exe container ls```

Executez commande psql: ```docker exec -it {{id}} psql clients postgres```

Executez la commande ```\copy client(id,prenom,nom,adresse,ville,code_postal) from 'var/lib/postgres/data/clients.csv' CSV```

**TP réalisé par Damien LE BORGNE - 2022**

