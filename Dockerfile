FROM openjdk:16-alpine3.13

EXPOSE 8080

RUN mkdir -p /app/
ADD build/libs/Client-0.0.1-SNAPSHOT.jar /app/client-back.jar

ENTRYPOINT ["java", "-jar","/app/client-back.jar"]
